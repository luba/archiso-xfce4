#!/bin/bash

# This is an old customization script called by previous versions of the archiso scripts during iso creation. According to arch wiki, the whole process is now different. Changes originally performed by this script (locale, timezone, adding users, passwords, enabling systemd units, etc.) now need to be added manually to the 'skeleton' directory structure 'airootfs'.
# build iso:
#   sudo mount tmpfs -t tmpfs -o size=6G work/
#   sudo mkarchiso -v -w work/ -o out/ ./
# Lukas Baron, 14 Sep 2020

set -e -u

sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

ln -sf /usr/share/zoneinfo/UTC /etc/localtime

usermod -s /usr/bin/zsh root
cp -aT /etc/skel/ /root/
chmod 700 /root

sed -i 's/#\(PermitRootLogin \).\+/\1yes/' /etc/ssh/sshd_config
sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf

sed -i 's/#\(HandleSuspendKey=\)suspend/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleHibernateKey=\)hibernate/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleLidSwitch=\)suspend/\1ignore/' /etc/systemd/logind.conf

systemctl enable pacman-init.service choose-mirror.service
systemctl set-default graphical.target

systemctl enable lightdm.service NetworkManager.service
systemctl enable vboxservice.service

# add live system user + groups + sudo
useradd -m -p "" -g users -G "adm,audio,floppy,log,network,rfkill,scanner,storage,optical,power,wheel" -s /usr/bin/zsh arch || echo "User exists"
echo '%wheel ALL=(ALL) ALL' >> /etc/sudoers

# autologin
groupadd -r autologin || echo "Group exists"
usermod -aG autologin arch
sed -i 's/^#\(autologin-user=\)$/\1arch/' /etc/lightdm/lightdm.conf
sed -i 's/^#\(autologin-session=\)$/\1xfce/' /etc/lightdm/lightdm.conf

# set passwords
echo "root:live" | chpasswd
echo "arch:live" | chpasswd
cp -aT /etc/skel/ /home/arch/
chown -R arch:users /home/arch

# cowsize pretty small (256MB)
# xz compression slows buil process down

# firefox smooth scrolling
echo MOZ_USE_XINPUT2=1 >> /etc/environment

# bash alises
cat << 'EOF' >> /home/arch/.zshrc
alias cowsize2G='sudo mount -o remount,size=2G /run/archiso/cowspace'
alias trizen-installer='sudo pacman -Sy && git clone https://aur.archlinux.org/trizen-git.git && cd trizen-git && makepkg -si --noconfirm'
EOF
