# features
* based on original arch iso configuration (releng)
* graphical user interface with virtualbox additions (use vboxsvga)
* autologin to xfce
* desktop environments: xfce, i3-gaps, sway
* browsers: firefox, dillo
* terminal tools: tmux, htop, vim, ncdu, tree
* multimedia: mpv with youtube-dl integration and hw acceleration up to intel coffelake
* urxvt terminal with paper color theme
* git + base-devel packages
* nmtui for network connection from terminal
* gparted

# custom configurations
* bash aliases: trizen-installer, cowsize2G
* basic xfce configuration with top panel

# login
username: arch, pw: live

# increase size of live root filesystem
* add kernel parameter: cow_spacesize=2G
* or run in live environment: mount -o remount,size=2G /run/archiso/cowspace
* or use the alias: cowsize2G

# create iso
sudo ./build.sh -v
